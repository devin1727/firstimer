package theater;
import java.util.*;
import ticket.Ticket;
import movie.Movie;
public class Theater{
	private String nama;
	private int kasAwal;
	private ArrayList<Ticket> listTicket;
	private Movie[] listMovie;
	
	public String getNama(){
		return this.nama;
	}
	public int getKasAwal(){
		return this.kasAwal;
	}
	public ArrayList<Ticket> getListTicket(){
		return this.listTicket;
	}
	public Movie[] getListMovie(){
		return this.listMovie;
	}
	
	public Theater (String nama, int kasAwal, ArrayList<Ticket> listTicket, Movie[] listMovie){
		this.nama = nama;
		this.kasAwal = kasAwal;
		this.listTicket = listTicket;
		this.listMovie = listMovie;
	}
	public void tambahKas(int jumlah){
		this.kasAwal += jumlah;
	}
	public void printInfo(){
		System.out.println("------------------------------------------------------------------");
		System.out.println("Bioskop                 : " + this.nama);
		System.out.println("Saldo Kas               : " + this.kasAwal);
		System.out.println("Jumlah tiket tersedia   : " + this.listTicket.size());
		System.out.print("Daftar Film tersedia    : ");
		for (int i = 0;i < this.listMovie.length;i++){
			if (i < this.listMovie.length - 1){
				System.out.print(this.listMovie[i].getJudul() + ", ");
			}
			else {
				System.out.println(this.listMovie[i].getJudul());
			}
		}
		System.out.println("------------------------------------------------------------------");
	}
	public static void printTotalRevenueEarned(Theater[] param){
		int total = 0;
		for (int i = 0; i < param.length; i++){
			total += param[i].getKasAwal();
		}
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + total);
		System.out.println("------------------------------------------------------------------");
		for (int i = 0;i < param.length; i++){
			System.out.println("Bioskop     : " + param[i].nama);
			System.out.println("Saldo Kas   : Rp. " + param[i].kasAwal + "\n");
		}
		System.out.println("------------------------------------------------------------------");
	}
}