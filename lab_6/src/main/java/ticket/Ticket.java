package ticket;
import movie.Movie;
public class Ticket {
	private Movie film;
	private String jadwal;
	private boolean tigaDimensi;
	private int harga;
	
	public Movie getFilm(){
		return this.film;
	}
	public String getJadwal(){
		return this.jadwal;
	}
	public boolean getTigaDimensi(){
		return this.tigaDimensi;
	}
	public int getHarga(){
		return this.harga;
	}
	
	public Ticket(Movie film, String jadwal, boolean Dimensi){
		this.film = film;
		this.jadwal = jadwal;
		this.tigaDimensi = Dimensi;
		if (this.jadwal.equals("Sabtu") || this.jadwal.equals("Minggu")){
			this.harga = 100000;
		}
		else {
			this.harga = 60000;
		}
		if (this.tigaDimensi == true){
			this.harga = this.harga + (int)(this.harga*0.2);
		}
	}
}