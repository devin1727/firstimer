package customer;
import movie.Movie;
import ticket.Ticket;
import theater.Theater;
public class Customer{
	private String nama;
	private int umur;
	private boolean jenisKelamin;
	
	public Customer(String nama, boolean jenisKelamin, int umur){
		this.nama = nama;
		this.jenisKelamin = jenisKelamin;
		this.umur = umur;
	}
	public Movie findMovie(Theater bioskop, String film){
		Movie ada = null;
		for (int i = 0;i < bioskop.getListMovie().length;i++){
			Movie yang_dicari = bioskop.getListMovie()[i];
			if (yang_dicari.getJudul().equals(film)){
				ada = yang_dicari;
				System.out.println("Judul   : " + bioskop.getListMovie()[i].getJudul());
				System.out.println("Genre   : " + bioskop.getListMovie()[i].getGenre());
				System.out.println("Durasi  : " + bioskop.getListMovie()[i].getDurasi());
				System.out.println("Rating  : " + bioskop.getListMovie()[i].getRating());
				System.out.println("Jenis   : Film " + bioskop.getListMovie()[i].getJenis());
				return ada;
			}
		}
		if (ada == null){
			System.out.println("Film " + film + " yang dicari " + this.nama + " tidak ada di bioskop " + bioskop.getNama());
			return ada;
		}
		return null;
	}
	public Movie findMovie1(Theater bioskop, String film){
		Movie ada = null;
		for (int i = 0;i < bioskop.getListMovie().length;i++){
			Movie yang_dicari = bioskop.getListMovie()[i];
			if (yang_dicari.getJudul().equals(film)){
				ada = yang_dicari;
				return ada;
			}
		}
		if (ada == null){
			System.out.println("Film " + film + " yang dicari " + this.nama + " tidak ada di bioskop " + bioskop.getNama());
			return ada;
		}
		return null;
	}
	public boolean cekLayak (Movie film){
		int batas;
		String rating = film.getRating();
		if (rating.equals("Umum")){
			return true;
		}
		else if (rating.equals("Remaja")){
			batas = 13;
			if (this.umur >= batas){
				return true;
			}
			else{
				return false;
			}
		}
		else if (rating.equals("Dewasa")){
			batas = 17;
			if (this.umur >= batas){
				return true;
			}
			else{
				return false;
			}
		}
		return false;
	}
	public Ticket orderTicket(Theater bioskop,String film, String hari, String Dimensi){
		boolean tiga = false;
		boolean y = false;
		if (Dimensi.equals("3 Dimensi")){
			tiga = true;
		}
		for (int i = 0;i < bioskop.getListTicket().size();i++){
			Ticket tiket = bioskop.getListTicket().get(i);
			if (tiket.getFilm().getJudul().equals(film)){
				if (tiket.getJadwal().equals(hari)){
					if (tiket.getTigaDimensi() == tiga){
						if (this.cekLayak(this.findMovie1(bioskop,film)) == true){
							System.out.println(this.nama + " telah membeli tiket " + film + " jenis " + Dimensi +" di " + bioskop.getNama() + " pada hari " + hari + " seharga Rp. " + tiket.getHarga());
							bioskop.tambahKas(tiket.getHarga());
							y = true;
							return tiket;
						}
						else {
							y = true;
							Movie apa = tiket.getFilm();
							System.out.println(this.nama + " masih belum cukup umur untuk menonton " + film + " dengan rating " + apa.getRating());
							return null;
						}
					}
					else {
						continue;
					}
				}
				else {
					continue;
				}
			}
			else {
				continue;
			}
		}
		if (y == false){
			System.out.println("Tiket untuk film " + film + " jenis " + Dimensi + " dengan jadwal " + hari + " tidak tersedia di " + bioskop.getNama());
		}
		return null;
	}
}