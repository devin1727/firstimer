package movie;
public class Movie{
	private String judul;
	private String genre;
	private int durasi;
	private String rating;
	private String jenis;
	
	public String getJudul(){
		return this.judul;
	}
	public String getGenre(){
		return this.genre;
	}
	public int getDurasi(){
		return this.durasi;
	}
	public String getRating(){
		return this.rating;
	}
	public String getJenis(){
		return this.jenis;
	}
	public Movie(String judul,String rating,int durasi,String genre,String jenis){
		this.judul = judul;
		this.rating = rating;
		this.durasi = durasi;
		this.genre = genre;
		this.jenis = jenis;
	}
}