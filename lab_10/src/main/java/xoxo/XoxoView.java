package xoxo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {
    JFrame frame;
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;
    private JLabel messageLabel;
    private JLabel keyLabel;
    private JLabel seedLabel;
    private JLabel logLabel;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        this.frame = new JFrame();
        this.frame.setLayout(new BorderLayout());
        this.messageField = new JTextField(10);  
        this.keyField = new JTextField(10);
        this.seedField = new JTextField("DEFAULT_SEED",10);
        this.logField = new JTextArea(10,10);
        this.encryptButton = new JButton("Encrypt");
        this.decryptButton = new JButton("Decrypt");
        this.messageLabel = new JLabel("Message          :");
        this.keyLabel = new JLabel("Key                    :");
        this.seedLabel = new JLabel("Seed                 :");
        this.logLabel = new JLabel("Log :");
        this.frame.setResizable(false);
        this.frame.setSize(500,300);
        this.keyField.setSize(300,50);
        this.messageField.setSize(300,100);
        this.logField.setSize(500,100);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    public JTextField getMessageField() { return messageField; }

    public JTextField getKeyField() { return keyField; }

    public JTextArea getLogField() { return logField; }

    public JButton getEncryptButton() { return encryptButton; }

    public JButton getDecryptButton() { return decryptButton; }

    public JLabel getMessageLabel() { return messageLabel; }

    public JLabel getKeyLabel() { return keyLabel; }

    public JLabel getLogLabel() { return logLabel; }

    public JTextField getSeedField() { return seedField; }

    public JLabel getSeedLabel() { return seedLabel; }

}