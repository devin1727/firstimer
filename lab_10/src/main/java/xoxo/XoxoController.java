package xoxo;
import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.frame.add(CreateMessagePanel(), BorderLayout.PAGE_START);
        gui.setEncryptFunction(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String message = gui.getMessageText();
                String key = gui.getKeyText();
                String stringSeed = gui.getSeedText();
                XoxoEncryption encryption = new XoxoEncryption(key);
                if (stringSeed.equals("")){
                    try{
                        gui.appendLog(encryption.encrypt(gui.getMessageText()).getEncryptedMessage());
                    }
                    catch (SizeTooBigException e1) {
                        e1.printStackTrace();
                    }
                }
                else {
                    try{
                        gui.appendLog(encryption.encrypt(gui.getMessageText(),Integer.parseInt(stringSeed)).getEncryptedMessage());
                    }
                    catch(SizeTooBigException e1){
                        e1.printStackTrace();
                    }
                }
            }
        });
        gui.setDecryptFunction(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String message = gui.getMessageText();
                String key = gui.getKeyText();
                String stringSeed = gui.getSeedText();
                XoxoDecryption decryption = new XoxoDecryption(key);
                if (stringSeed.equals("")){
                    gui.appendLog(decryption.decrypt(message,HugKey.DEFAULT_SEED));
                }
                else {
                    gui.appendLog(decryption.decrypt(message,Integer.parseInt(stringSeed)));
                }
            }
        });
    }

    public JPanel CreateMessagePanel (){
        JPanel messagePanel = new JPanel();
        messagePanel.setLayout(new GridLayout(3,2));
        messagePanel.add(gui.getMessageLabel());
        messagePanel.add(gui.getMessageField());
        messagePanel.add(gui.getKeyLabel());
        messagePanel.add(gui.getKeyField());
        messagePanel.add(gui.getSeedLabel());
        messagePanel.add(gui.getSeedField());

        JPanel ButtonPanel = new JPanel();
        ButtonPanel.setLayout(new BorderLayout());
        ButtonPanel.add(gui.getEncryptButton(), BorderLayout.PAGE_START);
        ButtonPanel.add(gui.getDecryptButton(), BorderLayout.PAGE_END);

        JPanel InputPanel = new JPanel();
        InputPanel.add(messagePanel);
        InputPanel.add(ButtonPanel);

        JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new BorderLayout());
        outputPanel.add(gui.getLogLabel(), BorderLayout.PAGE_START);
        outputPanel.add(gui.getLogField(), BorderLayout.PAGE_END);


        JPanel MainPanel = new JPanel();
        MainPanel.setLayout(new BorderLayout());
        MainPanel.add(InputPanel, BorderLayout.PAGE_START);
        MainPanel.add(outputPanel, BorderLayout.PAGE_END);

        return MainPanel;
    }
}