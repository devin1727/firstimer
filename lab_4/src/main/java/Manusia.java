//package Manusia;

public class Manusia {
	private String nama;
	private int umur;
	private int uang;
	private float bahagia = 50;
	
	public String getNama (){
		return this.nama;
	}
	public int getUmur (){
		return this.umur;
	}
	public int getUang (){
		return this.uang;
	}
	public float getKebahagiaan (){
		return this.bahagia;
	}
	public void setNama (String nama){
		this.nama = nama;
	}
	public void setUmur (int umur){
		this.umur = umur;
	}
	public void setUang (int uang){
		this.uang = uang;
	}
	public void setKebahagiaan (float bahagia){
		this.bahagia = bahagia;
	}
	public Manusia(String nama,int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
	}	

	public Manusia(String nama,int umur){
		this(nama,umur,50000);
	}
	public Manusia(String nama){
		this(nama,0,50000);
	}
	public void beriUang(Manusia penerima){
		float jumlah = 0;
		for (int i = 0; i < penerima.nama.length(); i++){
			int huruf = penerima.nama.charAt(i);
			jumlah += huruf;
		}
		jumlah = jumlah * 100;
		if (this.uang >= jumlah) {
			float kebahagiaan = jumlah/6000;
			this.uang = this.uang - (int)jumlah;
			penerima.uang += (int)jumlah;
			if (this.bahagia + kebahagiaan > 100) {
				this.bahagia = 100;
			}
			else {
				this.bahagia += kebahagiaan;
			}
			if (penerima.bahagia + kebahagiaan > 100) {
				penerima.bahagia = 100;
			}
			else {
				penerima.bahagia += kebahagiaan;
			}
			System.out.println(this.nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.nama +", mereka berdua senang :D");
		}
		else {
			System.out.println(this.nama +" ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
		}
	}
	
	public void beriUang(Manusia penerima,int jumlah){
		if (this.uang >= jumlah) {
			float kebahagiaan = (float)jumlah/6000;
			this.uang = this.uang - jumlah;
			penerima.uang += jumlah;
			if (this.bahagia + kebahagiaan > 100) {
				this.bahagia = 100;
			}
			else {
				this.bahagia += kebahagiaan;
			}
			if (penerima.bahagia + kebahagiaan > 100) {
				penerima.bahagia = 100;
			}
			else {
				penerima.bahagia += kebahagiaan;
			}
			System.out.println(this.nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.nama +", mereka berdua senang :D");
		}
		else {
			System.out.println(this.nama +" ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
		}
	}
	
	public void bekerja(float durasi,float beban) {
		if (this.umur < 18) {
			System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
		}
		else {
			float bebanTotal = durasi * beban;
			if (bebanTotal <= this.bahagia) {
				float kebahagiaan = bebanTotal;
				int pendapatan = (int)bebanTotal * 10000;
				this.uang += pendapatan;
				this.bahagia -= bebanTotal;
			}
			else {
				float durasiBaruFloat = this.bahagia / beban;
				int durasiBaru = (int)durasiBaruFloat;
				bebanTotal = durasiBaru * beban;
				int pendapatan = (int)bebanTotal * 10000;
				int kebahagiaan = durasiBaru * (int)beban;
				this.uang += pendapatan;
				if (this.bahagia - kebahagiaan <= 0) {
					this.bahagia = 0;
					System.out.println(this.nama +" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "+pendapatan);
				}
				else {
					this.bahagia -= kebahagiaan;
					System.out.println(this.nama +" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "+pendapatan);
				}
			}
		}
	}
	public void rekreasi(String tempat){
		int biaya = tempat.length() * 10000;
		float kebahagiaan = tempat.length();
		if (this.uang >= biaya) {
			this.uang -= biaya;
			if (this.bahagia + kebahagiaan > 100) {
				this.bahagia = 100;
			}
			else {
				this.bahagia += kebahagiaan;
			}
			System.out.println(this.nama +" berekreasi di " + tempat + ", " + this.nama + " senang :)");
		}
		else {
			System.out.println(this.nama +" tidak mempunyai cukup uang untuk berekreasi di "+tempat+" :(");
		}
	}
	public void sakit(String penyakit){
		float kebahagiaan = penyakit.length();
		if (this.bahagia - kebahagiaan < 0) {
			this.bahagia = 0;
			System.out.println(this.nama + " terkena penyakit " + penyakit + " :O");
		}
		else {
			this.bahagia -= kebahagiaan;
			System.out.println(this.nama + " terkena penyakit " + penyakit + " :O");
		}
	}
	
	
	
	
	public String toString(){
		return ("Nama		: " + this.nama + "\n" + "Umur		: " + this.umur + "\n" + "Uang		: " + this.uang + "\n" + "Kebahagiaan	: " + this.bahagia + "\n");
	}
}