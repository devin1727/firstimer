import java.util.Arrays;
import java.util.Scanner;

public class BingoCard {
	private String number = "";
	private String[] Line1 = new String[5];
	private String[] Line2 = new String[5];
	private String[] Line3 = new String[5];
	private String[] Line4 = new String[5];
	private String[] Line5 = new String[5];
	private String[] LineBaru1 = new String[5];
	private String[] LineBaru2 = new String[5];
	private String[] LineBaru3 = new String[5];
	private String[] LineBaru4 = new String[5];
	private String[] LineBaru5 = new String[5];
	private String[][] Card = new String[5][5];
	private String[][] CardBaru = new String[5][5];
	private boolean Bingo = false;
	
	public BingoCard (String param) {
		String[] splitparam = param.split(" ");
		this.number = param;
		String[] paramsplit = param.split(" ");
		this.Line1 = Arrays.copyOfRange(splitparam, 0, 5);
		this.Line2 = Arrays.copyOfRange(splitparam, 5, 10);
		this.Line3 = Arrays.copyOfRange(splitparam, 10, 15);
		this.Line4 = Arrays.copyOfRange(splitparam, 15, 20);
		this.Line5 = Arrays.copyOfRange(splitparam, 20, 25);
		this.LineBaru1 = Arrays.copyOfRange(paramsplit, 0, 5);
		this.LineBaru2 = Arrays.copyOfRange(paramsplit, 5, 10);
		this.LineBaru3 = Arrays.copyOfRange(paramsplit, 10, 15);
		this.LineBaru4 = Arrays.copyOfRange(paramsplit, 15, 20);
		this.LineBaru5 = Arrays.copyOfRange(paramsplit, 20, 25);
		this.Card[0] = Line1;
		this.Card[1] = Line2;
		this.Card[2] = Line3;
		this.Card[3] = Line4;
		this.Card[4] = Line5;
		this.CardBaru[0] = LineBaru1;
		this.CardBaru[1] = LineBaru2;
		this.CardBaru[2] = LineBaru3;
		this.CardBaru[3] = LineBaru4;
		this.CardBaru[4] = LineBaru5;
	}
	public boolean checkBingo(){
		for (int i = 0;i < 5;i++){
			if (this.CardBaru[i][0].equals("X")){
				int count = 1;
				for (int x = 1;1 < 5;x++){
					if (this.CardBaru[i][x].equals("X")){
						count++;
						if (count == 5) {
							return true;
						}
						}
					else {
						break;
					}
					}
				}
			else{
				continue;
			}
		}
		for (int i = 0;i < 5; i++){
			int y = 0;
			if (this.CardBaru[y][i].equals("X")){
				int count = 1;
				for (int x = 1;x < 5;x++){
					if (this.CardBaru[x][i].equals("X")){
						count++;
						if (count == 5) {
							return true;
							}
						}
					else {
						break;
						}
					}
				}
			}
		if (this.CardBaru[0][0].equals("X")){
			int count = 1;
			int x = 1;
			for (int i = 1;i<5;i++){
				if (this.CardBaru[x][x].equals("X")){
					count++;
					x++;
					if (count == 5) {
						return true;
					}
				}
				else {
					break;
				}
			}
		}
		if (this.CardBaru[4][4].equals("X")){
			int count = 1;
			int x = 3;
			for (int i = 1;i<5;i++){
				if (this.CardBaru[x][x].equals("X")){
					count++;
					x--;
					if (count == 5) {
						return true;
					}
				}
				else {
					break;
				}
			}
		}
		return false;
		}
	
	public void	markNum(String param) {
		int Value = 1;
		for (int i = 0; i < this.CardBaru.length;i++){
			for (int y = 0; y < this.CardBaru[i].length;y++){
				if (param.equals(this.CardBaru[i][y])) {
					if (this.CardBaru[i][y].equals("X")){
						Value = 3;
						}
					else {
						this.CardBaru[i][y] = "X";
						Value = 2;
						}
				}
				else {
					continue;
				}
			}
		}
		if (Value == 1) {
			System.out.println("Kartu tidak memiliki angka " + param);
		}
		else if (Value == 2){
			System.out.println(param + " tersilang");
		}
		else {
			System.out.println(param + " sebelumnya sudah tersilang");
		}
	}
	
	public void info(){
		for (int i = 0; i < this.Card.length;i++){
			for (int y = 0; y < this.Card[i].length;y++){
				if (y == 4) {
					System.out.print("| "+this.CardBaru[i][y] + " | \n");
				}
				else {
					System.out.print("| " + this.CardBaru[i][y]+ " ");
				}
			}
		}
	}
	
	public void restart(){
		this.CardBaru = this.Card.clone();
		System.out.println("Mulligan!");
	}
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		String semua = "";
		for (int i = 1; i < 6; i++){
			System.out.println("Masukkan baris ke-" + i + ": ");
			String input = reader.nextLine();
			semua = semua + input + " ";
		}
		BingoCard Kartu = new BingoCard(semua);
		while (Kartu.checkBingo() == false) {
			String input = reader.nextLine();
			String[] splitInput = input.split(" ");
			if (splitInput[0].equals("MARK")){
				Kartu.markNum(splitInput[1]);
			}
			else if (splitInput[0].equals("INFO")){
				Kartu.info();
			}
			else if (splitInput[0].equals("RESTART")){
				Kartu.restart();
			}
			else {
				System.out.println("Incorrect command");
			}
			}
		System.out.println("BINGO!");
		Kartu.info();
		}
	}

