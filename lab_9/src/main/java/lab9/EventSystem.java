package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.*;
import java.math.*;
import java.io.*;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    public Event getEvent(String name) {
        for (Event i : events) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }
    public User getUser(String name) {
        for (User i : users) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        for (Event a:events){
            if (a.getName().equals(name)){
                return ("Event " + name + " sudah ada!");  
            }
        }
        String[] waktuStart = startTimeStr.split("_");
        String[] waktuStop = endTimeStr.split("_");
        String[] hariStart = waktuStart[0].split("-");
        String[] jamStart = waktuStart[1].split(":");
        String[] hariStop = waktuStop[0].split("-");
        String[] jamStop = waktuStop[1].split(":");
        Date dateStart = new Date(Integer.parseInt(hariStart[0]),Integer.parseInt(hariStart[1]),Integer.parseInt(hariStart[2]),Integer.parseInt(jamStart[0]),Integer.parseInt(jamStart[1]),Integer.parseInt(jamStart[2]));
        Date dateStop = new Date(Integer.parseInt(hariStop[0]),Integer.parseInt(hariStop[1]),Integer.parseInt(hariStop[2]),Integer.parseInt(jamStop[0]),Integer.parseInt(jamStop[1]),Integer.parseInt(jamStop[2]));
        if (dateStart.after(dateStop)){
            return ("Waktu yang di input tidak valid!");
        }
        else {
            Event param = new Event(name,dateStart,dateStop, new BigInteger(costPerHourStr));
            this.events.add(param);
            return ("Event " + name + " telah berhasil ditambahkan!");
        }
    }
    public String addUser(String name)
    {
        for (User a:users){
            if (a.getName().equals(name)){
                return ("User " + name + " sudah ada!");
            }
        }
        User param = new User(name);
        this.users.add(param);
        return ("User " + name + " berhasil ditambahkan");
    }
    public String registerToEvent(String userName, String eventName)
    {
        User param = null;
        Event param1 = null;
        for (User a:users){
            if (a.getName().equals(userName)){
                param = a;
                break;
            }
        }
        for (Event d:events){
            if (d.getName().equals(eventName)){
                param1 = d;
                break;
            }
        }
        if (param == null & param1 == null){
            return ("Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!");
        }
        else if (param == null){
            return ("Tidak ada pengguna dengan nama " + userName + "!");
        }
        else if (param1 == null){
            return ("Tidak ada event dengan nama " + eventName + "!");
        }
        else {
            if (param.addEvent(param1)){
                return (userName + " berencana menghadiri " + eventName + "!");
            }
            else {
                return (userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!");
            }
        }
    }
}