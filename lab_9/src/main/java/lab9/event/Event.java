package lab9.event;

import java.util.*;
import java.io.*;
import java.math.*;

public class Event
{
    private String name;
    
    // TODO: Make instance variables for representing beginning and end time of event
    private Date start;
    private Date stop;
    // TODO: Make instance variable for cost per hour
    private BigInteger cost;
    // TODO: Create constructor for Event class
    public Event(String name,Date start, Date stop, BigInteger cost){
        this.name = name;
        this.start = start;
        this.stop = stop;
        this.cost = cost;
    }

    public String getName()
    {
        return this.name;
    }
    public Date getStart()
    {
        return this.start;
    }
    public Date getStop()
    {
        return this.stop;
    }
    public BigInteger getCost()
    {
        return this.cost;
    }
    public String toString(){
        return (this.name + "\nWaktu mulai: " + Integer.toString(this.start.getDate()) + "-" + Integer.toString(this.start.getMonth()) + "-" + Integer.toString(this.start.getYear()) + " , " + Integer.toString(this.start.getHours()) + ":" + Integer.toString(this.start.getMinutes()) + ":" + Integer.toString(this.start.getSeconds()) + "\n"
                +"Waktu selesai: " + Integer.toString(this.stop.getDate()) + "-" + Integer.toString(this.stop.getMonth()) + "-" + Integer.toString(this.stop.getYear()) + " , " + Integer.toString(this.stop.getHours()) + ":" + Integer.toString(this.stop.getMinutes()) + ":" + Integer.toString(this.stop.getSeconds()) + "\n"
                +"Biaya Kehadiran: " + this.cost);
    }
    public boolean overlapsWith(Event param){
        if (param.getStart().before(this.getStop())){
            return false;
        }
        else if (param.getStart().after(this.getStart())){
            return false;
        }
        else {
            return true;
        }
    }
} 
