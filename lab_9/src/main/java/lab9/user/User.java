package lab9.user;

import lab9.event.Event;
import java.util.*;
import java.math.*;

/**
* Class representing a user, willing to attend event(s)
*/
public class User
{
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private ArrayList<Event> events;
    private BigInteger totalCost = new BigInteger("0");
    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }
    public void setCost(BigInteger cost){
        this.totalCost = cost;
    }
    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }
    public BigInteger getTotalCost(){
        BigInteger totalCost = new BigInteger("0");
        for (Event i: events){
            setCost(this.totalCost.add(i.getCost()));
        } return this.totalCost;
    }
    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    *
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent)
    {
        // TODO: Implement!
        for (Event a:events){
            if (newEvent.overlapsWith(a)){
                return false;
            }
        }
        this.events.add(newEvent);
        return true;
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents()
    {
        ArrayList<Event> param = new ArrayList<Event>();
        for (Event a:this.events){
            param.add(a);
        }
        Collections.sort(param, (start1,start2) -> start1.getStart().compareTo(start2.getStart()));
        return param;
    }
}
