import java.util.Scanner;
public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.println("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		String panjang = input.nextLine();
		int Pa = Integer.parseInt(panjang);
		System.out.print("Lebar Tubuh (cm)       : ");
		String lebar = input.nextLine();
		int Le = Integer.parseInt(lebar);
		System.out.print("Tinggi Tubuh (cm)      : ");
		String tinggi = input.nextLine();
		int Ti = Integer.parseInt(tinggi);
		System.out.print("Berat Tubuh (kg)       : ");
		String berat = input.nextLine();
		float Be = Float.parseFloat(berat);
		System.out.print("Jumlah Anggota Keluarga: \n");
		String makanan = input.nextLine();
		int makan = Integer.parseInt(makanan);
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		String jumlahCetakan = input.nextLine();
		int jumlah = Integer.parseInt(jumlahCetakan);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		double rasio = (Be / ((Pa/100.0)*(Le/100.0)*(Ti/100.0)));
		int ras = (int) rasio;

		for (int i = 1;i <= jumlah;i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase
			penerima = penerima.toUpperCase();

			// TODO Periksa ada catatan atau tidak
			if (catatan != null && !catatan.isEmpty()){
				catatan = "Catatan: " + catatan;
			}
			else {catatan = "Tidak Ada Catatan";
			}

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			System.out.println("DATA SIAP DICETAK UNTUK " + penerima + "\n" + 
				"--------------------\n" +
				nama + " - " + alamat + "\n" +
				"Lahir pada tanggal " + tanggalLahir + "\n" +
				"Rasio Berat per Volume = " + ras + " kg/m^3\n" +
				catatan);
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int noKeluarga = (Pa * Ti * Le);
		for (int i = 0; i < nama.length(); i++) {
			int z = nama.charAt(i);
			noKeluarga += z;
			}
		noKeluarga = noKeluarga % 10000;

		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String number = Integer.toString(noKeluarga);
		String nomorKeluarga = nama.charAt(0) + number;

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000 * 365 * makan;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String tanggal[] = tanggalLahir.split("-");
		int tahun = Integer.parseInt(tanggal[2] );
		int umur = 2018 - tahun;

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String tempat = "";
		String kabupaten = "";
		if (umur > 0 && umur < 18) {
			tempat = "PPMT";
			kabupaten = "Rotunda";
		}
		else if ( anggaran < 0 && anggaran > 100000000){
			tempat = "Teksas";
			kabupaten = "Sastra";
		}
		else {
			tempat = "Mares";
			kabupaten = "Margonda";
		}




		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		System.out.println("REKOMENDASI APARTEMEN\n" +
				"--------------------\n" +
				"MENGETAHUI: Identitas keluarga: " + nama + "-" + nomorKeluarga + "\n" +
				"MENIMBANG:  Anggaran makanan tahunan: Rp " + anggaran + "\n" +
				"            Umur kepala keluarga: " + umur + " tahun\n" +
				"MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" +
				tempat + ", kabupaten " + kabupaten);

		input.close();
	}
}