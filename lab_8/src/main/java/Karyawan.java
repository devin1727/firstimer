import java.util.*;
public class Karyawan {
	private String nama;
	private String tipe;
	private int gajiAwal;
	private int gaji;
	private int jumlahGajian;
	private ArrayList<Karyawan> bawahan;

	public Karyawan(String nama,int gajiAwal){
		this.nama = nama;
		this.gajiAwal = gajiAwal;
		this.gaji = 0;
		this.jumlahGajian = 0;
		bawahan = new ArrayList<Karyawan>();
	}
	public int getJumlahBawahan(){
		return this.bawahan.size();
	}
	public String getName(){
		return this.nama;
	}
	public String getTipe(){
		return this.tipe;
	}
	public int getGaji(){
		return this.gaji;
	}
	public int getGajiAwal(){
		return this.gajiAwal;
	}
	public int getJumlahGajian(){
		return this.jumlahGajian;
	}
	public void setJumlahGajian(int param){
		this.jumlahGajian = param;
	}
	public void setTipe(String param){
		this.tipe = param;
	}
	public void setGajiAwal(int param){
		this.gajiAwal = param;
	}
	public void addGaji(int param){
		this.gaji += param;
		this.jumlahGajian += 1;
	}

	public void addBawahan(Karyawan param){
		if (this.getTipe().equals("Manager")){
			if (param.getTipe().equals("Manager")){
				System.out.println("Anda tidak layak memiliki bawahan");
			}
			else if (param.getJumlahBawahan() < 10){
				this.bawahan.add(param);
				System.out.println("Karyawan " + param.getName() + " telah menjadi bawahan " + this.getName());
			}
		}
		else if (this.getTipe().equals("Staff")){
			if (param.getTipe().equals("Intern") & param.getJumlahBawahan() < 10){
				this.bawahan.add(param);
				System.out.println("Karyawan " + param.getName() + " telah menjadi bawahan " + this.getName());
			}
			else {
				System.out.println("Anda tidak layak memiliki bawahan");
			}
		}
		else {
			System.out.println("Anda tidak layak memiliki bawahan");
		}
	}
	public void promosi(){
		this.tipe = "Manager";
	}
}