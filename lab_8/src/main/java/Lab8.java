import java.util.Scanner;
class Lab8 {
    public static void main(String[] args) {
    	Scanner reader = new Scanner(System.in);
    	Perusahaan Tampan = new Perusahaan();
    	while (true) {
    		String input = reader.nextLine();
    		String[] input_split = input.split(" ");
    		if (input_split[0].equals("TAMBAH_KARYAWAN")){
    			if (input_split[2].equals("MANAGER")){
    				Karyawan x = new Manager(input_split[1],Integer.parseInt(input_split[3]));
    				Tampan.addKaryawan(x);
    			}
    			else if (input_split[2].equals("STAFF")){
    				Karyawan x = new Staff(input_split[1],Integer.parseInt(input_split[3]));
    				Tampan.addKaryawan(x);
    			}
    			else if (input_split[2].equals("INTERN")){
    				Karyawan x = new Intern(input_split[1],Integer.parseInt(input_split[3]));
    				Tampan.addKaryawan(x);
    			}
    		}
    		else if (input_split[0].equals("GAJIAN")){
    			Tampan.Gajian();
    		} 
    		else if (input_split[0].equals("STATUS")){
    			Tampan.printStatus(input_split[1]);
    		}
    		else if (input_split[0].equals("TAMBAH_BAWAHAN")){
    			Tampan.assignBawahan(input_split[1],input_split[2]);
    		}
    	}
    }
}