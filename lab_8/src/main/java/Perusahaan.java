import java.util.*;
public class Perusahaan{
	private ArrayList<Karyawan> pekerja;

	public Perusahaan(){
		this.pekerja = new ArrayList<Karyawan>();
	}
	public Karyawan find(String param){
		boolean ada = false;
		for (Karyawan p:this.pekerja){
			if (p.getName().equals(param)){
				ada = true;
				return p;
			}
		}
		if (ada = false){
			return null;
		}
		return null;
	}
	public void addKaryawan(Karyawan param){
		if (this.find(param.getName()) == null){
			this.pekerja.add(param);
			System.out.println(param.getName() + " telah bekerja sebagai " + param.getTipe() + " di PT. TAMPAN");
		}
		else {
			System.out.println("Karyawan dengan nama " + param.getName() +" telah terdaftar");
		}
	}
	public void assignBawahan(String param,String param1){
		Karyawan atasan = this.find(param);
		Karyawan bawahan = this.find(param1);
		if (atasan != null & bawahan != null){
			atasan.addBawahan(bawahan);
		}
		else {
			System.out.println("Nama tidak berhasil ditemukan");
		}
	}
	public void printStatus(String param){
		Karyawan x = this.find(param);
		if (x == null){
			System.out.println("Karyawan tidak ditemukan");
		}
		else {
			System.out.println(x.getName() + " " + x.getGaji());
		}
	}
	public void Gajian(){
		for (Karyawan p:pekerja){
			p.addGaji(p.getGajiAwal());
		}
		System.out.println("Semua karyawan telah diberikan gaji");
		for (Karyawan p:pekerja){
			if (p.getJumlahGajian() == 6){
				int var = p.getGajiAwal();
				p.setJumlahGajian(0);
				p.setGajiAwal(p.getGajiAwal() + (p.getGajiAwal() * 10 / 100));
				System.out.println(p.getName() + " mengalami kenaikan gaji sebesar 10% dari " + var + " menjadi " + p.getGajiAwal());
				if (p.getGajiAwal() > 18000){
					p.promosi();
					System.out.println("Selamat, " + p.getName() + " telah di promosikan menjadi MANAGER");
				}
			}
			else if (p.getGajiAwal() > 18000){
					p.promosi();
					System.out.println("Selamat, " + p.getName() + " telah di promosikan menjadi MANAGER");
			}
		}
	}

}