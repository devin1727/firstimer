import character.*;
import java.util.*;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        boolean ada = false;
        Player value = null;
        for (Player d:player){
            String nama = d.getName();
            if (name.equals(nama)){
                value = d;
                ada = true;
                break;
            }
        }
        if (ada == true){
            return value;
        }
        else {
            return null;
        }
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        Player karakter = this.find(chara);
        if (karakter == null){
            if (tipe.equals("Human")){
                Player baru = new Human(chara,tipe,hp);
                player.add(baru);
            }
            else if (tipe.equals("Magician")){
                Player baru = new Magician(chara,tipe,hp);
                player.add(baru);
            }
            else if (tipe.equals("Monster")){
                Player baru = new Monster(chara,tipe,hp);
                player.add(baru);
            }
            return (chara + " telah ditambah ke dalam game");
        }
        else {
            return ("Sudah ada karakter bernama " + chara);
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        Player karakter = this.find(chara);
        if (karakter == null){
            if (tipe.equals("Human")){
                Player baru = new Human(chara,tipe,hp);
                player.add(baru);
            }
            else if (tipe.equals("Magician")){
                Player baru = new Magician(chara,tipe,hp);
                player.add(baru);
            }
            else if (tipe.equals("Monster")){
                Player baru = new Monster(chara,tipe,hp,roar);
                player.add(baru);
            }
            return (chara + " telah ditambah ke dalam game");
        }
        else {
            return ("Sudah ada karakter bernama " + chara);
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        Player karakter = this.find(chara);
        if (karakter != null){
            player.remove(karakter);
            return (chara + "telah dihapus dari game");
        }
        else {
            return ("Tidak ada " + chara);
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        Player karakter = this.find(chara);
        String nama = karakter.getName();
        String tipe = karakter.getTipe();
        int darah = karakter.getHp();
        String Diet = "Memakan ";
        String hidup = "";
        if (darah == 0){
            hidup = "Sudah meninggal dunia dengan damai";
        }
        else {
            hidup = "Masih hidup";
        }
        ArrayList<Player> makanan = karakter.getDiet();
        if (makanan.size() == 0){
            Diet = "Belum memakan siapa siapa";
        }
        else {
            for (Player d:makanan){
            String x = d.getName();
            Diet = Diet + x + " ";
            }
        }
        return (tipe + " " + nama + "\n" + "HP: " + darah + "\n" + hidup + "\n" + Diet + "\n");
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String hasil = "";
        for (Player d:player){
            String nama = d.getName();
            hasil += this.status(nama);
        }
        return (hasil);     
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        String hasil = "Memakan ";
        Player karakter = this.find(chara);
        ArrayList<Player> makanan = karakter.getDiet();
        if (makanan.size() == 0){
            hasil = "Belum memakan siapa siapa";
        }
        else {
            for (Player d:makanan){
            String x = d.getName();
            hasil = hasil + x + " "; 
            }
        }
        return hasil;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String hasil = "Memakan ";
        for (Player d:player){
            String nama = d.getName();
            ArrayList<Player> makanan = d.getDiet();
            for (Player f:makanan){
                String name = f.getName();
                if (makanan.size() == 0){
                hasil = "Belum memakan siapapun";
                }
                else {
                    hasil += name + " ";
                }
            }
        }
        return (hasil);
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player penyerang = this.find(meName);
        Player terserang = this.find(enemyName);
        int HPterserang = terserang.getHp();
        if (terserang.getTipe().equals("Magician")){
            terserang.setHP(HPterserang - 20);
            return (terserang.getName() + " " + terserang.getHp() + "\nNyawa " + terserang.getHp() + " kini " + terserang.getHp());
        }
        else {
            terserang.setHP(HPterserang - 10);
            return (terserang.getName() + " " + terserang.getHp());
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        String tambahan = "\n dan matang";
        Player penyerang = this.find(meName);
        Player terserang = this.find(enemyName);
        if (penyerang.getTipe().equals("Magician")){ 
            if (terserang.getTipe().equals("Magician")){
                terserang.setHP(terserang.getHp() - 20);
                if (terserang.getHp() == 0){
                    terserang.setMatang(true);
                    return (terserang.getName() + " " + terserang.getHp() + tambahan);
                }
                else {
                    return (terserang.getName() + " " + terserang.getHp());
                }
            }
            else {
                terserang.setHP(terserang.getHp() - 10);
                if (terserang.getHp() == 0){
                    terserang.setMatang(true);
                    return (terserang.getName() + " " + terserang.getHp() + tambahan);
                }
                else {
                    return (terserang.getName() + " " + terserang.getHp());
                }
            }
        }
        else {
            return (penyerang.getName() + " tidak bisa melakukan burn");
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player penyerang = this.find(meName);
        Player terserang = this.find(enemyName);
        if (penyerang.canEat(terserang)){
            penyerang.setHP(penyerang.getHp() + 15);
            penyerang.addDiet(terserang);
            return (penyerang.getName() + " memakan " + terserang.getName() + "\nNyawa " + penyerang.getName() + " kini " + penyerang.getHp());
        }
        else {
            return (penyerang.getName() + " tidak bisa memakan " + terserang.getName());
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player karakter = this.find(meName);
        if (karakter == null){
            return ("Tidak ada " + meName);
        }
        else {
            return (karakter.roar());
        }
    }
}