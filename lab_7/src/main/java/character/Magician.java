package character;

public class Magician extends Player{
	public Magician(String nama,String tipe,int hp){
		super(nama,tipe,hp);
	}
	public Magician(String nama,int hp){
		super(nama,"Magician",hp);
	}
}