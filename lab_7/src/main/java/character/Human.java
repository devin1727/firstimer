package character;

public class Human extends Player{
	public Human(String nama,String tipe,int hp){
		super(nama,tipe,hp);
	}
	public Human(String nama,int hp){
		super(nama,"Human",hp);
	}
}