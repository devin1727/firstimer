package character;
import java.util.*;
public class Player{
	private String nama;
	private String tipe;
	private int hp;
	private ArrayList<Player> diet = new ArrayList<Player>();
	private boolean matang;

	public Player(String nama,String tipe,int hp){
		this.nama = nama;
		this.tipe = tipe;
		this.hp = hp;
	}
	public String getName(){
		return this.nama;
	}
	public String getTipe(){
		return this.tipe;
	}
	public int getHp(){
		return this.hp;
	}
	public ArrayList<Player> getDiet(){
		return this.diet;
	}
	public void setHP(int param){
		if (param < 0){
			this.hp = 0;
		}
		else {
			this.hp = param;
		}
	}
	public void addDiet(Player param){
		this.diet.add(param);
	}
	public boolean isMatang(){
		return this.matang;
	}
	public void setMatang(boolean param){
		this.matang = param;
	}
	public String roar(){
		return (this.nama + " tidak bisa berteriak");
	}
	public boolean canEat(Player param){
		if (this.tipe.equals("Human") || this.tipe.equals("Magician")){
			if (param.getTipe().equals("Monster") & param.isMatang() == true){
                return true;
			}
			else {
				return false;
			}
		}
		else {
			if (param.getTipe().equals("Human") || param.getTipe().equals("Magician")){
				return true;
			}
			else {
				return false;
			}
		}
	}
}
