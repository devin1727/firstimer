package character;

public class Monster extends Player{
	private String roar;

	public Monster(String nama,String tipe,int hp){
		super(nama,tipe,2*hp);
		this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
	}
	public Monster(String nama,int hp){
		super(nama,"Monster",2*hp);
		this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
	}
	public Monster(String nama,String tipe,int hp,String roar){
		super(nama,tipe,2*hp);
		this.roar = roar;
	}
	public String roar(){
		return this.roar;
	}
}